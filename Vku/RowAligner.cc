///////////////////////////////////////////////////////
// RowAligner.cc
//////////////////////////////////////////////////////// 

#include <strstream>

#include <Xm/Form.h>

#include "RowAligner.hh"

String RowAligner::_defaultResources[] = {
	"*?.resizable: True",
    NULL
};
 
RowAligner::RowAligner(Widget parent, char * name, int numRows) 
	: VkComponent(name),
	  mNumRows(0),
	  mHorSpacing(0)
{
    setDefaultResources(parent, _defaultResources);
	
	_baseWidget = XmCreateForm(parent, _name, NULL, 0);

	installDestroyHandler();

	for (int i = 0; i < numRows; ++i) 
	{
		addRow();
	}
}

RowAligner::~RowAligner()
{
	// Empty
}

void 
RowAligner::addRow()
{
	char buff[1024];
	ostrstream rowName(buff, 1024);
	rowName << "row" << mNumRows << ends;
	XmCreateForm(_baseWidget, rowName.str(), NULL, 0);
	mNumRows++;
}

Widget 
RowAligner::getRow(unsigned int rowNum)
{
	if (rowNum < mNumRows) 
	{
		WidgetList wl;
		XtVaGetValues(_baseWidget, XmNchildren, &wl, NULL);
		return wl[rowNum];
	}
	return NULL;
}

void 
RowAligner::doLayout()
{
	unsigned int i, j;
	
	// Get the list of row Forms
	WidgetList rowList;
	XtVaGetValues(_baseWidget, XmNchildren, &rowList, NULL);

	unsigned int* numItems = new unsigned int[mNumRows];

	// Find the maximumum number of items in the rows
	unsigned int maxItems = 0;
	for (i = 0; i < mNumRows; ++i) 
	{
		unsigned int ni;
		XtVaGetValues(rowList[i], XmNnumChildren, &ni, NULL);
		numItems[i] = ni;
		maxItems = ni > maxItems ? ni : maxItems;
	}

	// Initialise the max widths and offsets arrays
	int* maxItemWidth = new int[maxItems];
	int* offsets = new int[maxItems];
	for (i = 0; i < maxItems; ++i)
	{
		maxItemWidth[i] = offsets[i] = 0;
	}

	// Populate the max widths array
	for (i = 0; i < mNumRows; ++i) 
	{
		WidgetList itemList;
		XtVaGetValues(rowList[i], XmNchildren, &itemList, NULL);
		XtManageChildren(itemList, numItems[i] );
		for (j = 0; j < numItems[i]; ++j)
		{
			Dimension width;
			XtVaGetValues(itemList[j], XmNwidth, &width, NULL);
			maxItemWidth[j] = width>maxItemWidth[j] ? width : maxItemWidth[j];
		}
	}
	
	// Calculate the offsets for each item
	for (i = 1; i < maxItems; ++i)
	{
		offsets[i] = offsets[i - 1] + maxItemWidth[i - 1] + mHorSpacing;
	}

	for (i = 0; i < mNumRows; ++i) 
	{
		// Attach to the row Form above
		XtVaSetValues(
			rowList[i],
			XmNtopAttachment,
			i == 0 ? XmATTACH_FORM : XmATTACH_WIDGET,
			XmNtopWidget,
			i == 0 ? NULL : rowList[i - 1],
			XmNbottomAttachment,
			i == mNumRows - 1 ? XmATTACH_FORM : XmATTACH_NONE,
			XmNleftAttachment, XmATTACH_FORM,
			XmNrightAttachment, XmATTACH_FORM,
			NULL);
		// Get the list of items in this row
		WidgetList itemList;
		XtVaGetValues(rowList[i], XmNchildren, &itemList, NULL);
		for (j = 0; j < numItems[i]; ++j)
		{
			// Attach each item at its left offset from the form
			XtVaSetValues(
				itemList[j],
				XmNtopAttachment, XmATTACH_FORM,
				XmNbottomAttachment, XmATTACH_FORM,
				XmNleftAttachment, XmATTACH_FORM,
				XmNleftOffset, offsets[j],
				XmNrightAttachment, XmATTACH_NONE,
				NULL);
		}
	}
	XtManageChildren(rowList, mNumRows);

	delete [] numItems;
	delete [] maxItemWidth;
	delete [] offsets;
}

void
RowAligner::afterRealizeHook()
{
	doLayout();
}
