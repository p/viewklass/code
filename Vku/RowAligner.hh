///////////////////////////////////////////////////////
// RowAligner.hh
//////////////////////////////////////////////////////// 

#ifndef ROWALIGNER_H
#define ROWALIGNER_H

#include <Vk/VkComponent.h>

/**
 * A container which aligns the children of each of its rows.
 */
class RowAligner : public VkComponent {

public:
	/**
	 * Default ViewKit constructor.
	 */
	RowAligner(Widget parent, char *name, int numRows = 0); 
	
	/**
	 * Destructor.
	 */
	virtual ~RowAligner();

	/**
	 * Return the class name for Viewkit resource management.
	 */
	virtual const char* className() { return "RowAligner"; }

	/**
	 * Add a row.
	 */
	void	addRow();

	/**
	 * Get the container widget for a row.
	 * @param rowNum The row for which to return the widget
	 * @return The container widget for the row.
	 */
	Widget	getRow(unsigned int rowNum);

	/**
	 * Set the space, in pixels, between the widgets in a row.
	 * @param space The number of pixels to separate adjacent widgets.
	 */
	void	setHorSpacing(unsigned int space) { mHorSpacing = space; }

	/**
	 * Layout all the widgets in the RowAligner.
	 * This method must be called after all the widgets have been added.
	 */
	void	doLayout();

	/**
	 * Calls doLayout.
	 */
	virtual void afterRealizeHook();

private:
	// Copy and assignment not allowed

	/**
	 * RowAligner can not be copied
	 */
	RowAligner::RowAligner(const RowAligner& from);

	/**
	 * RowAligner can not be assigned
	 */
	RowAligner& operator=(const RowAligner& rhs);
	
	/**
	 *  The default resources.
	 */
    static String _defaultResources[];

	/**
	 * The number of rows.
	 */
	unsigned int mNumRows;

	/**
	 * The space between each widget in a row, in pixels.
	 */
	unsigned int mHorSpacing;
};

#endif


