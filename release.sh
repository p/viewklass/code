#!/bin/sh -x

#
# Copy all the files to the release directory
#

rm -rf ../release.$1
mkdir -p ../release.$1/Vk
mkdir -p ../release.$1/test

cp *C ../release.$1
cp COPYING INSTALL Makefile.in configure configure.in install.sh ../release.$1
cp Vk/*h* ../release.$1/Vk
cp test/*[ch] ../release.$1/test
cp test/Vktest ../release.$1/test
cp test/StaticMenuTest ../release.$1/test
cp test/ResourceTest ../release.$1/test
cp test/Makefile ../release.$1/test
cp test/README ../release.$1/test
cp test/*xpm ../release.$1/test

#
# Copy all the files to the test build directory
#
rm -rf ../testbuild.$1
mkdir -p ../testbuild.$1/Vk
mkdir -p ../testbuild.$1/test

cp *C ../testbuild.$1
cp COPYING INSTALL Makefile.in configure configure.in install.sh ../testbuild.$1
cp Vk/*h* ../testbuild.$1/Vk
cp test/*[ch] ../testbuild.$1/test
cp test/Vktest ../testbuild.$1/test
cp test/StaticMenuTest ../testbuild.$1/test
cp test/ResourceTest ../testbuild.$1/test
cp test/Makefile ../testbuild.$1/test
cp test/README ../testbuild.$1/test
cp test/*xpm ../testbuild.$1/test

